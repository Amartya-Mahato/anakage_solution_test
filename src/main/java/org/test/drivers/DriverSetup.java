package org.test.drivers;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.test.global.Global;
import org.test.providers.Provider;
import org.testng.Assert;
import org.testng.annotations.Test;
public class DriverSetup {
    @Test(groups = {"setup"})
    public static void setUp() {
        Provider.setExtentReports();
        Provider.test = Provider.extent.createTest("Driver Setup");
        ExtentTest test = Provider.test;

        test.log(Status.INFO, "Collecting Drivers");
        FirefoxOptions fOptions = new FirefoxOptions();
        ChromeOptions cOptions = new ChromeOptions();
        EdgeOptions eOptions = new EdgeOptions();
        SafariOptions sOptions = new SafariOptions();

        try {
            Provider.driver = new FirefoxDriver(fOptions);
            test.log(Status.PASS, "Initializing Firefox Driver");
        } catch (Exception firefox) {
            try {
                Provider.driver = new ChromeDriver(cOptions);
                test.log(Status.PASS, "Initializing Chrome Driver");
            } catch (Exception chrome) {
                try {
                    Provider.driver = new EdgeDriver(eOptions);
                    test.log(Status.PASS, "Initializing Edge Driver");
                } catch (Exception edge) {
                    try {
                        Provider.driver = new SafariDriver(sOptions);
                        test.log(Status.PASS, "Initializing Safari Driver");
                    } catch (Exception safari) {
                        test.log(Status.FAIL, "Supported Driver is not available");
                        test.fail("Supported Browser is not installed in your machine");
                        Assert.fail("Supported Browser is not installed in your machine");
                    }
                }
            }
        }
        try {
            Provider.driver.get(Global.LOGIN_SCREEN_URL);
            test.log(Status.PASS, "Switching to Anakage Homepage");
        } catch (Exception urlException) {
            System.err.println("Couldn't reach the http://dev.anakage.com/login");
            test.log(Status.FAIL, "Couldn't reach the http://dev.anakage.com/login");
            test.fail("Couldn't reach the http://dev.anakage.com/login");
            Assert.fail("Couldn't reach the http://dev.anakage.com/login");
        }
    }
}
