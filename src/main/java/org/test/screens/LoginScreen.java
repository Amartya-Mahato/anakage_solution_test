package org.test.screens;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.test.global.Global;
import org.test.providers.Provider;
import org.testng.annotations.Test;

import java.time.Duration;
public class LoginScreen {
    @Test(groups = {"login"}, dependsOnGroups = {"setup"})
    static void loginTest() {
        Provider.test = Provider.extent.createTest("Login Screen");
        ExtentTest test = Provider.test;
        WebDriver driver = Provider.driver;

        //UserName TextBox Element
        WebElement userNameElement = driver.findElement(By.id("userName"));
        test.log(Status.INFO, "Found Username Text Box");
        userNameElement.sendKeys(Global.USERNAME);

        //Password TextBox Element
        WebElement passwordElement = driver.findElement(By.name("j_password"));
        test.log(Status.INFO, "Found Password Text Box");
        passwordElement.sendKeys(Global.PASSWORD + Keys.ENTER);

        //Checking if the homepage is Displayed
        try {
            test.log(Status.INFO, "Switching to Homepage");
            new WebDriverWait(driver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.id("home")));
            test.log(Status.PASS, "Inside Homepage");
        } catch (Exception e) {
            test.log(Status.FAIL, "Failed to login or Taking long time to login");
            test.fail("Couldn't Login the User properly");
            Assert.fail("Couldn't Login the User properly");
        }

    }

}
