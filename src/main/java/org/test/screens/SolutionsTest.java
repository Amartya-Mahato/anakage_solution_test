package org.test.screens;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.opencsv.CSVReader;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.test.providers.Provider;
import org.test.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.FileReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
public class SolutionsTest {
    @Test(groups = {"sol"}, dependsOnGroups = {"login", "dash"})
    public static void newlyAddedSolutionsTableTest() {
        Provider.test = Provider.extent.createTest("Solutions Screen");
        ExtentTest test = Provider.test.createNode("Newly added solution table test");

        WebDriver driver = Provider.driver;
        try {
            //Waiting for the NEWLY_ADDED_SOLUTIONS section to be displayed [ just waiting for the NEWLY ADDED SOLUTIONS text to be displayed ]
            Assert.assertTrue(Utils.idWait(driver, "table-caption-id12"));
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.id("table-caption-id12")));
            test.log(Status.PASS, "Found NEWLY ADDED SOLUTIONS");
            //Scrolling to NEWLY_ADDED_SOLUTIONS
            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});",
                    driver.findElement(By.xpath("//*[@id=\"solutionDiv\"]/div[1]/div[2]")));
            test.log(Status.INFO, "Scrolled to NEWLY ADDED SOLUTIONS");

            //Fetching table values
            WebElement tableElement = driver.findElement(By.xpath("//*[@id=\"sample_12\"]"));
            List<List<String>> table = new ArrayList<>();
            for (WebElement row : tableElement.findElements(By.tagName("tr"))) {
                List<String> col = new ArrayList<>();
                for (WebElement cell : row.findElements(By.tagName("td"))) {
                    col.add(cell.getText());
                }
                table.add(col);
            }

            Provider.table = table;
            if (!table.isEmpty()) {
                test.log(Status.PASS, "Fetched all values form newly added solutions table");
            } else {
                test.log(Status.FAIL, "Newly added solutions table is empty");
                Assert.fail("Newly added solutions table is empty");
            }
        } catch (Exception e) {
            test.log(Status.FAIL, e.toString());
            Assert.fail(e.toString());
        }
    }

    @Test(priority = 1, groups = {"sol"}, dependsOnGroups = {"login", "dash"})
    public static void csvRead() {
        ExtentTest test = Provider.test.createNode("CSV Reading");

        try {
            test.log(Status.INFO, "Started reading the csv file");
            //Reading the csv file present ins assets/csv/test.csv
            FileReader fileReader = new FileReader("assets/csv/test.csv");
            CSVReader csvReader = new CSVReader(fileReader);
            Provider.csvFile = csvReader.readAll();
            if (Provider.csvFile.isEmpty()) {
                test.log(Status.FAIL, "CSV is Empty");
                test.fail("Csv file is Empty");
                Assert.fail("Csv file is Empty");
            }
            test.log(Status.PASS, "Successfully read the CSV file");
        } catch (Exception e) {
            test.log(Status.FAIL, "Failed to read");
            test.fail("Couldn't find the CSV file in assets");
            Assert.fail("Couldn't find the CSV file in assets");
        }
    }

    @Test(priority = 2, dependsOnMethods = {"csvRead"}, groups = {"sol"}, dependsOnGroups = {"login", "dash"})
    public static void countTest() {
        ExtentTest test = Provider.test.createNode("Count Test");
        List<String[]> csvFile = Provider.csvFile;
        List<List<String>> table = Provider.table;
        try {
            //testing the count of the lines in csv file and in the newly added solutions
            if (table.size() == csvFile.size()) {
                test.log(Status.PASS, "Total number of solutions in Csv file ( csv solutions -> \" + (csvFile.size() - 1) + \" ) and in Newly Added Solutions ( newly added solutions -> \" + (table.size() - 1) + \" ) are same");
                System.out.println("Total number of solutions in Csv file ( csv solutions -> " + (csvFile.size() - 1) + " ) and in Newly Added Solutions ( newly added solutions -> " + (table.size() - 1) + " ) are same");
            } else {
                test.log(Status.FAIL, "Total number of solutions in Csv file ( csv solutions -> \" + (csvFile.size() - 1) + \" ) and in Newly Added Solutions ( newly added solutions -> \" + (table.size() - 1) + \" ) are different");
                test.fail("Total number of solutions in Csv file ( csv solutions -> \" + (csvFile.size() - 1) + \" ) and in Newly Added Solutions ( newly added solutions -> \" + (table.size() - 1) + \" ) are different");
                Assert.fail("Total number of solutions in Csv file ( csv solutions -> " + (csvFile.size() - 1) + " ) and in Newly Added Solutions ( newly added solutions -> " + (table.size() - 1) + " ) are different");
            }
        } catch (Exception e) {
            test.log(Status.FAIL, e.toString());
            Assert.fail(e.toString());
        }
    }

    @Test(priority = 3, dependsOnMethods = {"countTest"}, groups = {"sol"}, dependsOnGroups = {"login", "dash"})
    public static void solutionNumberTest() {
        ExtentTest test = Provider.test.createNode("Solution Number Test");
        List<String[]> csvFile = Provider.csvFile;
        List<List<String>> table = Provider.table;

        List<Integer> matched = new ArrayList<>();
        List<Integer> unmatched = new ArrayList<>();

        try {
            test.log(Status.INFO, "Started comparing values between CSV file and Newly added solution table");
            for (int i = 1; i < csvFile.size(); i++) {
                boolean isMatched = false;
                int csvNumber = (int) (Float.parseFloat(csvFile.get(i)[5].trim().isEmpty() ? "0" : csvFile.get(i)[5]));
                for (int j = 1; j < table.size(); j++) {
                    int tableNumber = (int) Float.parseFloat(table.get(j).get(5).trim().isEmpty() ? "0" : table.get(j).get(5));
                    if (tableNumber == csvNumber) {

                        matched.add(csvNumber);
                        isMatched = true;
                        break;
                    }
                }
                if (!isMatched) {
                    unmatched.add(csvNumber);
                }

            }
            test.log(Status.PASS, "Compared csv files with newly added solutions table");
            Provider.matched = matched;
            Provider.unmatched = unmatched;
        } catch (Exception e) {
            test.log(Status.FAIL, e.toString());
            Assert.fail(e.toString());
        }
    }
}
