package org.test.screens;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.test.providers.Provider;
import org.test.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;
public class DashBoardScreen {
    @Test(groups = {"dash"}, dependsOnGroups = {"login"})
    public static void dashBoardScreenTest() {
        Provider.test = Provider.extent.createTest("Dashboard Screen");
        ExtentTest test = Provider.test.createNode("Finding Dashboard Button");

        WebDriver driver = Provider.driver;
        try {
            //Waiting for this dashboard button to be displayed
            Assert.assertTrue(Utils.xpathWait(driver, "//*[@id=\"pageHeaderHeight\"]/div[2]/div/div[1]/ul/li[2]"));

            //dashboard button
            driver.findElement(By.xpath("//*[@id=\"pageHeaderHeight\"]/div[2]/div/div[1]/ul/li[2]")).click();
            test.log(Status.PASS, "Found Dashboard button");
            test.log(Status.INFO, "Pressed on Dashboard button");
        } catch (Exception e) {
            test.log(Status.FAIL, e.toString());
            Assert.fail(e.toString());
        }
    }

    @Test(priority = 1, groups = {"dash"}, dependsOnGroups = {"login"})
    public static void contentManagementButtonTest() {
        ExtentTest test = Provider.test.createNode("Finding Content Management Button");
        WebDriver driver = Provider.driver;

        try {
            //Waiting for the content management button to be displayed
            Assert.assertTrue(Utils.idWait(driver, "version"));

            //Content Management button
            driver.findElement(By.id("version")).click();
            test.log(Status.PASS, "Found Content Management Button");
            test.log(Status.INFO, "Pressed on Content Management Button");
        } catch (Exception e) {
            test.log(Status.FAIL, e.toString());
            Assert.fail(e.toString());
        }
    }

    @Test(priority = 2, groups = {"dash"}, dependsOnGroups = {"login"})
    public static void solutionButtonTest() {
        ExtentTest test = Provider.test.createNode("Finding Solution Button");
        WebDriver driver = Provider.driver;
        try {
            //Waiting for the solution button to be displayed
            Assert.assertTrue(Utils.idWait(driver, "solutions"));

            //solution button
            driver.findElement(By.id("solutions")).click();
            test.log(Status.PASS, "Found Solution Button");
            test.log(Status.INFO, "Pressed Solution Button");
        } catch (Exception e){
            test.log(Status.FAIL,e.toString());
            Assert.fail(e.toString());
        }
    }
}
