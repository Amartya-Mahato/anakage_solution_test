package org.test.utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.test.providers.Provider;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class Results {
    @Test(groups = {"result"})
    public static void generateResults() {

        Provider.test = Provider.extent.createTest("Results");
        ExtentTest test = Provider.test;
        if(Provider.matched.isEmpty()) {
            test.log(Status.PASS, "Nothing to be matched");
        }
            //Creating log for matched and unmatched numbers
            test.createNode("Matched Numbers")
                    .log(Provider.matched.isEmpty() ? Status.FAIL : Status.PASS, MarkupHelper.toTable(Provider.matched, "table-sm"));

            test.createNode("Unmatched Numbers")
                    .log(Status.FAIL, MarkupHelper.toTable(Provider.unmatched, "table-sm"));

        Provider.extent.flush();

        //Printing matched and unmatched numbers
        System.out.println("Matched Numbers: " + Provider.matched);
        System.out.println("Unmatched Numbers: " + Provider.unmatched);

        //Creating new tab for the results
        Provider.driver.findElement(By.tagName("body")).sendKeys(Keys.CONTROL + "t");
        Provider.driver.switchTo().window(Provider.driver.getWindowHandle()).get("file:///C://Reports/test-output/report_result.html");

    }
}
