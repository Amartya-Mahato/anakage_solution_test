package org.test.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class Utils {
    public static boolean elementWait(WebDriver driver, WebElement element) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> element.isDisplayed());
    }
    public static boolean xpathWait(WebDriver driver, String xPath) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> webDriver.findElement(By.xpath(xPath)).isDisplayed());
    }
    public static boolean NameWait(WebDriver driver, String name) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> webDriver.findElement(By.name(name)).isDisplayed());
    }
    public static boolean idWait(WebDriver driver, String id) {
        return new WebDriverWait(driver, Duration.ofSeconds(10)).until(webDriver -> webDriver.findElement(By.id(id)).isDisplayed());
    }
    public static boolean contentWait(WebDriver driver, String content) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> webDriver.getPageSource().contains(content));
    }
    public static boolean urlWait(WebDriver driver, String url) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> webDriver.getCurrentUrl().contains(url));
    }
    public static boolean partialLinkWait(WebDriver driver, String partial) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> webDriver.findElement(By.partialLinkText(partial)).isDisplayed());
    }
    public static boolean linkWait(WebDriver driver, String link) {
        return new WebDriverWait(driver, Duration.ofSeconds(5)).until(webDriver -> webDriver.findElement(By.linkText(link)).isDisplayed());
    }
}
