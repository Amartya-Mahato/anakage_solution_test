package org.test.providers;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class Provider {
    public static WebDriver driver;
    public static ExtentReports extent = new ExtentReports();

    public static ExtentTest test;

    public static List<List<String>> table = new ArrayList<>();
    public static List<Integer> matched = new ArrayList<>();
    public static List<Integer> unmatched = new ArrayList<>();
    public static List<String[]> csvFile = new ArrayList<>();
    public static WebDriver getDriver() {
        return driver;
    }
    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }
    public static void setExtentReports() {
        extent = new ExtentReports();
        extent.attachReporter(new ExtentSparkReporter("C://Reports/test-output/report_result.html"));
    }

}
